package ru.t1.rleonov.tm.command.user;

import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserChangePasswordProfileCommand extends AbstractUserCommand {

    private final String NAME = "user-update-profile";

    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        final String userId = getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        getUserService().setPassword(userId, password);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
