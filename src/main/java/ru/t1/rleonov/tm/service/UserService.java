package ru.t1.rleonov.tm.service;

import ru.t1.rleonov.tm.api.repository.IProjectRepository;
import ru.t1.rleonov.tm.api.repository.ITaskRepository;
import ru.t1.rleonov.tm.api.repository.IUserRepository;
import ru.t1.rleonov.tm.api.service.IUserService;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.exception.entity.UserNotFoundException;
import ru.t1.rleonov.tm.exception.field.*;
import ru.t1.rleonov.tm.exception.user.*;
import ru.t1.rleonov.tm.model.User;
import ru.t1.rleonov.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public UserService(
            final IUserRepository userRepository,
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository
    ) {
        super(userRepository);
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(login, password);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new EmailExistsException();
        return repository.create(login, password, email);
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        return repository.create(login, password, role);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = repository.findByLogin(login);
        return user;
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = repository.findByEmail(email);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User remove(final User model) {
        if (model == null) return null;
        final User user = super.remove(model);
        if (user == null) return null;
        final String userId = user.getId();
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        return user;
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return remove(user);
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = repository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = repository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        user.setLocked(false);
    }

}
