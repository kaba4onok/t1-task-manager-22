package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    public final static String COMMAND = "task-unbind-from-project";

    public final static String DESCRIPTION = "Unbind task from project.";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        final String userId = getUserId();
        getProjectTaskService().unbindTaskFromProject(userId, projectId, taskId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
