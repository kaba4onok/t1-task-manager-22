package ru.t1.rleonov.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-v";

    public static final String NAME = "version";

    public static final String DESCRIPTION = "Show application version.";

    public static final String VERSION = "1.22.0";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(VERSION);
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
