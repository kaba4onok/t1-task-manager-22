package ru.t1.rleonov.tm.command.user;

import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    private final String NAME = "user-unlock";

    private final String DESCRIPTION = "Unlock user account";

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        getUserService().unlockUserByLogin(login);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Role[] getRoles() {
        return new Role[] { Role.ADMIN };
    }

}
