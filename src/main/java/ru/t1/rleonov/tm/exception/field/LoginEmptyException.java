package ru.t1.rleonov.tm.exception.field;

import ru.t1.rleonov.tm.exception.user.AbstractUserException;

public final class LoginEmptyException extends AbstractUserException {

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
