package ru.t1.rleonov.tm.command.project;

public final class ProjectClearCommand extends AbstractProjectCommand {

    public final static String COMMAND = "project-clear";

    public final static String DESCRIPTION = "Delete all projects.";

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return COMMAND;
    }

}
