package ru.t1.rleonov.tm.command.task;

import ru.t1.rleonov.tm.api.service.IProjectTaskService;
import ru.t1.rleonov.tm.api.service.ITaskService;
import ru.t1.rleonov.tm.command.AbstractCommand;
import ru.t1.rleonov.tm.enumerated.Role;
import ru.t1.rleonov.tm.enumerated.Status;
import ru.t1.rleonov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toDisplayName(task.getStatus()));
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
